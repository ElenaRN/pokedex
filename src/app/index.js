import { getAllPokemon , viewList} from "./API/api-for-list";
import{displayPokemonDetail}from "./VIEWS/view-detail-paint";

import{ viewSearch}from "./API/api-for-search-list-detail";
import{ viewComparator}from "./API/api-for-pokemon-comparator";
import{displayPokeComparator}from "./VIEWS/view-comparator";
import{viewAboutProyect} from "./info-proyect/about-proyect"
import{viewContactMe} from "./info-proyect/contact-me"
import "./styles/styles.scss";

var viewHome= () => {
  
  document.getElementById("home").classList.remove("hidden");
  document.getElementById("about").classList.add("hidden");
  document.getElementById("contact").classList.add("hidden"); 
  document.getElementById("listPoke").classList.add("hidden");
  document.getElementById("formSearchContainer").classList.add("hidden");
  document.getElementById("formComparator").classList.add("hidden");
};


function addListeners() {
  
  document.getElementById("btnHome").addEventListener("click", viewHome);
  document.getElementById("btnAbout").addEventListener("click", viewAboutProyect);
  document.getElementById("btnContact").addEventListener("click", viewContactMe);
  
  document.getElementById("anchorHome").addEventListener("click", viewHome);
  document.getElementById("btnList").addEventListener("click", viewList);
  document.getElementById("btnDetails").addEventListener("click", displayPokemonDetail);
  document.getElementById("btnSearch").addEventListener("click", viewSearch);
  document.getElementById("btnComparator").addEventListener("click", viewComparator);
  document.getElementById("btnComparatorInput").addEventListener("click", displayPokeComparator); 
  
}

window.onload = function () {
  
  addListeners();
  

};
