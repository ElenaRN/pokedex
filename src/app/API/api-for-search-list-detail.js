var viewSearch = () => {
    
  document.getElementById("home").classList.add("hidden");
  document.getElementById("listPoke").classList.add("hidden");
  document.getElementById("formSearchContainer").classList.remove("hidden");
  document.getElementById("resultSearch").classList.add("hidden");
  document.getElementById("formComparator").classList.add("hidden");
};

const pokemonSearch = async (pokeName) => {
    const url = `https://pokeapi.co/api/v2/pokemon/${pokeName}/`;
    
    const response = await fetch(url)
    
    .catch((error) => {
      const pokedex = document.getElementById("resultSearch");
      pokedex.classList.remove("hidden");
      const pokemonHTMLString = `
      <div class="section-list__pokeUl__container">
          <p class="section-list__pokeUl__container__error">Error: Tus pokemons están cansados, ve al centro pokemon: '${error.message}</p>
      </div>`;
      pokedex.innerHTML = pokemonHTMLString;
    });
    const dataPokemon = await response.json();
    let pokemon = {
      name: dataPokemon.name,
      image: dataPokemon.sprites["front_default"],
      type: dataPokemon.types.map((type) => type.type.name).join(", "),
      id: dataPokemon.id,
      attack: dataPokemon,
    };
    return pokemon;
  };
  

      export{viewSearch, pokemonSearch};
