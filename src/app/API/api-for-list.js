import {
    displayPokemons
} from "../VIEWS/view-list-paint"

var viewList = () => {
    document.getElementById("home").classList.add("hidden");
    document.getElementById("listPoke").classList.remove("hidden");
    document.getElementById("formComparator").classList.add("hidden");
    getAllPokemon();
};
//me devuelve todos los pokemons que quiero
const getAllPokemon = /* async */ () => {

    const petitions = [];
    //el bucle es para recoger los 150 pokemons primeros

    for (var i = 1; i <= 151; i++) {
        const urlPokemonApi = `https://pokeapi.co/api/v2/pokemon/${i}`;


        petitions.push(fetch(urlPokemonApi).then((response) => response.json()));
        //pusheamos al array vacio la url de pokeapi y lo convertimos en un json
    };
    /* console.log(promises); */
    //reolvemos con esto las promesas
    Promise.all(petitions).then((results) => { //results son los resultados que estamos recibiendo
        //guardo la respuesta del resultado en una const de pokemon y lo recorremos con map lo que quiero traerme
        const dataPokemons = results.map((result) => ({ //result es el objeto que me voy a traer a continuación
            name: result.name,
            img: result.sprites["front_default"],
            type: result.types.map((type) => type.type.name).join(', '), //es un array de 2 tipos , mapeo para traerme los dos por el nombre.
            id: result.id
        }));

        console.log(dataPokemons);
        displayPokemons(dataPokemons);

    })
};

export {
    getAllPokemon, viewList
};