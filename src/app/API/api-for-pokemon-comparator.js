 var viewComparator = () => {
  document.getElementById("home").classList.add("hidden");
  document.getElementById("listPoke").classList.add("hidden");
  document.getElementById("formComparator").classList.remove("hidden");
  document.getElementById("resultComparator").classList.add("hidden");
 };


 const searchPokemonComparator = async (name) => {
   const url = `https://pokeapi.co/api/v2/pokemon/${name}/`;

   const response = await fetch(url)

     .catch((error) => {

       const pokedex = document.getElementById("resultComparator");
       pokedex.classList.remove("hidden");
       const pokemonHTMLString = `
      <div class="flex-item">
          <p class="flex-item-error">Error: Has perdido el combate, ve al centro pokemon: '${error.message}</p>
      </div>`;
       pokedex.innerHTML = pokemonHTMLString;
     });

   const dataForComparator = await response.json();

   let pokemonComparator = {
     name: dataForComparator.name,
     image: dataForComparator.sprites["front_default"],
     type: dataForComparator.types.map((type) => type.type.name).join(", "),
     attack: dataForComparator,
     baseExperience: dataForComparator.base_experience,
   };

   return pokemonComparator;

 };

 export {
   searchPokemonComparator,
   viewComparator
 };