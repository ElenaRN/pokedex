import{searchPokemonComparator}from '../API/api-for-pokemon-comparator';
import { PokeComparator } from '../class/pokemon-class-list';
import { PokeUtil } from '../class/class-utils-comparator';

const displayPokeComparator = async () => {
    
    const pokedex = document.getElementById("resultComparator");
    pokedex.classList.remove("hidden");
    const comparate1 = await document.getElementById("comparator1").value;
    const comparate2 = await document.getElementById("comparator2").value;
    const pokemon1 = await searchPokemonComparator(comparate1)
    const pokemon2 = await searchPokemonComparator(comparate2)

    let pokemonWinner = PokeUtil.pokeLevel(pokemon1, pokemon2); 
    const myComparatorClass = new PokeComparator (pokemonWinner.id, pokemonWinner.name, pokemonWinner.image, pokemonWinner.type, pokemonWinner.baseExperience);
    const pokemonHTMLString =
        `<li class="section-list__pokeUl__li">
            <img class="section-list__pokeUl__li__item" src="${ myComparatorClass.getPokemonImg() }"/>
            <h2 class="section-list__pokeUl__li__item1">${myComparatorClass.getPokemonName() }</h2>
            <p class="section-list__pokeUl__li__item2">Type: ${ myComparatorClass.getPokemonType()} </p>
            <p class="section-list__pokeUl__li__item2">EXP: ${ myComparatorClass.getPokeExperience()}</p>
        </li>`
    pokedex.innerHTML = pokemonHTMLString;
    
};



export { displayPokeComparator }



