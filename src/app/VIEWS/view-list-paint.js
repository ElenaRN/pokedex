import { getAllPokemon } from "../API/api-for-list";

const displayPokemons = (pokemon) => {
  
  const pokedex = document.getElementById("listPokemons");
  
  const pokemonHTML = pokemon
    .map(
      (detailsPokemon) =>
    `
    
    <li class="section-list__pokeUl__li">
        <img class="section-list__pokeUl__li__item" src="${detailsPokemon.img}">
        <h3 class="section-list__pokeUl__li__item1">${detailsPokemon.id}. ${detailsPokemon.name}</h3>
        <p class="section-list__pokeUl__li__item2">${detailsPokemon.type}</p>
    </li>
    
    `
    )
    .join("");
  pokedex.innerHTML = pokemonHTML;
};

export { displayPokemons};
