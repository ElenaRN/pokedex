import { pokemonSearch } from '../API/api-for-search-list-detail';
import {PokemonClass, PokeDetailClass } from '../class/pokemon-class-list';



const displayPokemonDetail = async () => {
 
  const pokedex = document.getElementById('resultSearch');
  pokedex.classList.remove("hidden");
  const inputSearch = await document.getElementById('inputSearch').value;
  const pokemon = await pokemonSearch(inputSearch);
  const myPokemonClass = new PokeDetailClass(
    pokemon.id,
    pokemon.name,
    pokemon.image,
    pokemon.type,  
    pokemon.attack
  );
  const pokeAttacks = myPokemonClass.getPokemonAttacks();
  let attacksStrings = '';
  for (let i = 0; i < pokeAttacks.abilities.length; i++) {
    attacksStrings =
      attacksStrings + ' ' + i + ' ' + pokeAttacks.abilities[i].ability.name;
  }

  const pokemonHTMLString = `<li class="section-list__pokeUl__li">
            <img class="section-list__pokeUl__li__item" src="${myPokemonClass.getPokemonImg()}"/>
            <h3 class="section-list__pokeUl__li__item1">${myPokemonClass.getPokemonId()}. ${myPokemonClass.getPokemonName()}</h3>
            <p class="section-list__pokeUl__li__item2">Type: ${myPokemonClass.getPokemonType()}</p>
            <h3 class="section-list__pokeUl__li__item1"> Attacks:</h3>
            <p class="section-list__pokeUl__li__item2">${attacksStrings}</p>
        </li>`;
  pokedex.innerHTML = pokemonHTMLString;
  
  
};
 

  export {displayPokemonDetail};